# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).


Live Demo --> [live](https://reduxtest21.herokuapp.com/)


## In the first tab (Account Settings):

- Change e-mail address
- Change Password
- A password strength indicator should be implemented
- Button to update the user data

## In the second tab (User Information):

- Change first name
- Change Last Name
- Change address (street, house number, postal code)
- Change country (Germany, Austria, Switzerland are available)
- Button to update the user data

## The button uses a fake AJAX call to submit the information through






## Demo of the App

![My_Movie_8](/uploads/64e6a2d8956707b8462535062e358c15/My_Movie_8.mp4)


---






### Install Dependencies

```
npm install

npm start
```



