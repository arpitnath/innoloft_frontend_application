import React from 'react';

import Sidebar from '../Components/Sidebar';
import Tabs from '../Components/Tabs';

const Dashboard = () => {
	return (
		<>
			<Sidebar />
			<Tabs />
		</>
	);
};

export default Dashboard;
