import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
	AcUpdateForm,
	FormGroup,
	FormLabel,
	FormInput,
	SubmitButton,
	Message,
} from './styleComponents';
import { updateProfile } from '../actions/accountActions';

const Profile = () => {
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [address, setAddress] = useState('');
	const [country, setCountry] = useState('');
	const [message, setMessage] = useState(null);

	const profileUpdate = useSelector((state) => state.profileUpdate);
	const { profileInfo } = profileUpdate;

	const dispatch = useDispatch();

	useEffect(() => {
		if (profileInfo) {
			console.log(profileInfo);
			setMessage('Profile Updated');
		}
	}, [profileInfo]);

	const submitHandler = (e) => {
		e.preventDefault();
		// console.log('update');
		dispatch(updateProfile({ firstName, lastName, address, country }));
		setMessage(null);
	};
	return (
		<>
			<AcUpdateForm onSubmit={submitHandler}>
				<Message>{message}</Message>
				<FormGroup>
					<FormLabel>First Name</FormLabel>

					<FormInput
						type='text'
						placeholder='Enter your First Name'
						name='firstName'
						value={firstName}
						onChange={(e) => setFirstName(e.target.value)}
					/>
				</FormGroup>

				<FormGroup>
					<FormLabel>Last Name</FormLabel>
					<FormInput
						type='text'
						placeholder='Enter your lastname'
						name='lastname'
						value={lastName}
						onChange={(e) => setLastName(e.target.value)}
					/>
				</FormGroup>

				<FormGroup>
					<FormLabel>Address</FormLabel>
					<FormInput
						type='textarea'
						placeholder='street, house number, postal code'
						name='address'
						value={address}
						onChange={(e) => setAddress(e.target.value)}
					/>
				</FormGroup>
				<FormGroup>
					<FormLabel>Country</FormLabel>

					<FormInput
						type='text'
						as='select'
						name='country'
						value={country}
						// eslint-disable-next-line prettier/prettier
						onChange={(e) => setCountry(e.target.value)}>
						<option value='0'>Select...</option>
						<option value='Germany'>Germany</option>
						<option value='Austria'>Austria</option>
						<option value='Switzerland'>Switzerland</option>
					</FormInput>
				</FormGroup>

				<SubmitButton type='submit'>Update</SubmitButton>
			</AcUpdateForm>
		</>
	);
};

export default Profile;
