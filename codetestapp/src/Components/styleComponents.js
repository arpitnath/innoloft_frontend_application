import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const Header = styled.header`
	left: 0;
	position: fixed;
	width: 100%;
	z-index: 1000;
`;

export const Nav = styled.nav`
	background-color: #272e71;
	height: 60px;
	margin-top: -100px;
	align-items: stretch;
	display: flex;
`;

export const NavbarContainer = styled.div`
	display: flex;
	justify-content: space-between;
	height: 80px;
	z-index: 1;
	width: 100%;
	padding: 0 24px;
	max-width: 1100px;
`;

export const NavIcon = styled(Link)`
	margin-left: 2rem;
	color: white;
	font-size: 2rem;
	height: 60px;
	display: flex;
	justify-content: flex-start;
	align-items: center;
`;

export const NavLogo = styled(Link)`
	color: #efefef;
	justify-self: flex-start;
	cursor: pointer;
	font-size: 1.5rem;
	display: flex;
	align-items: center;
	margin-left: -200px;
	margin-top: 0px;
	font-weight: bold;
	text-decoration: none;

	@media screen and (max-width: 768px) {
		margin-left: 10px;
		align-items: center;
		margin-top: 0px;
	}
`;

export const NavItem = styled.li`
	height: 80px;
`;

export const SidebarNav = styled.div`
	background-color: #272e71;
	width: 250px;
	height: 100vh;
	display: flex;
	justify-content: center;
	position: fixed;
	top: 0;
	left: ${({ sidebar }) => (sidebar ? '0' : '-100%')};

	transition: 350ms;
	z-index: 10;
`;

export const SidebarWrap = styled.div`
	width: 100%;
`;

export const SidebarLink = styled(Link)`
	display: flex;
	color: white;
	justify-content: space-between;
	align-items: center;
	padding: 20px;
	list-style: none;
	height: 60px;
	text-decoration: none;
	font-size: 18px;

	&:hover {
		background-color: #252831;
		border-left: 4px solid #632ce4;
		cursor: pointer;
	}
`;

export const SidebarLabel = styled.span`
	margin-left: 16px;
`;

export const DropdownLink = styled(Link)`
	background-color: #413757;
	height: 60px;
	padding-left: 3rem;
	display: flex;
	align-items: center;
	text-decoration: none;
	color: #f5f5f5;
	font-size: 18px;

	&:hover {
		background-color: #632ce4;
		cursor: pointer;
	}
`;

export const AcUpdateForm = styled.form`
	position: absolute;
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);
	width: 50%;
	height: auto;
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	margin-top: 200px;
`;

export const FormGroup = styled.div`
	width: 100%;
	margin-bottom: 0.5rem;
`;

export const FormLabel = styled.label`
	display: inline-block;
	font-size: 1.2rem;
	margin-bottom: 6px;

	@media screen and (max-width: 768px) {
		font-size: 1rem;
	}
`;
export const FormInput = styled.input`
	display: block;
	outline: none;
	border-radius: 3px;
	height: 40px;
	width: 100%;

	@media screen and (max-width: 768px) {
		width: auto;
	}
`;

export const SubmitButton = styled.button`
	width: 100%;
	height: 50px;
	margin-top: 10px;
	border-radius: 3px;
	background: #272e71;
	color: #fff;
	outline: none;
	border: none;
	font-size: 1rem;

	&:hover {
		cursor: pointer;
		background: linear-gradient(
			90deg,
			rgb(39, 143, 255) 0%,
			rgb(12, 99, 250) 100%
		);
		transition: all 0.4s ease-out;
	}
`;
export const Message = styled.span`
	color: red;
	padding: 2px;
	margin-top: -10px;
	height: 30px;
	width: 100%;
	font-size: 1.3rem;

	text-align: center;
`;

export const ProgressBar = styled.div`
	background: rgba(255, 255, 255, 0.1);
	justify-content: flex-start;
	border-radius: 100px;
	align-items: center;
	position: relative;
	padding: 0 5px;
	display: flex;
	height: 40px;
	width: 100%;
	margin-top: 10px;
`;

export const ProgressValue = styled.div`
	animation: load 3s normal forwards;
	box-shadow: 0 10px 40px -10px #fff;
	border-radius: 100px;
	background: #fff;
	height: 30px;
	width: 0;
`;
