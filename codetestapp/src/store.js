import { createStore, combineReducers, applyMiddleware } from 'redux';

import thunk from 'redux-thunk';

import { composeWithDevTools } from 'redux-devtools-extension';
import {
	userAccountUpdateReducer,
	userProfileUpdateReducer,
} from './reducers/accountReducers';

const reducer = combineReducers({
	accountUpdate: userAccountUpdateReducer,
	profileUpdate: userProfileUpdateReducer,
});

const intialState = {};

const middleware = [thunk];

const store = createStore(
	reducer,
	intialState,
	composeWithDevTools(applyMiddleware(...middleware))
);

export default store;
