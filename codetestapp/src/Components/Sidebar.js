import React, { useState } from 'react';
import '../App.css';
import * as FaIcons from 'react-icons/fa';
import * as AiIcons from 'react-icons/ai';
import SubMenu from './Submenu';
import { SidebarData } from './SidebarData';

import {
	Nav,
	Header,
	NavbarContainer,
	NavIcon,
	NavLogo,
	NavItem,
	SidebarNav,
	SidebarWrap,
} from './styleComponents';

const Sidebar = () => {
	const [sidebar, setSidebar] = useState(false);

	const showSidebar = () => setSidebar(!sidebar);
	return (
		<>
			<Header>
				<Nav>
					<NavbarContainer>
						<NavIcon to='#'>
							<FaIcons.FaBars onClick={showSidebar} />
						</NavIcon>
						<NavItem>
							<NavLogo>Logo</NavLogo>
						</NavItem>
						<NavItem>
							<input className='search' type='text' placeholder='Search..' />
						</NavItem>
					</NavbarContainer>
				</Nav>
				<SidebarNav sidebar={sidebar}>
					<SidebarWrap>
						<NavIcon to='#'>
							<AiIcons.AiOutlineClose onClick={showSidebar} />
						</NavIcon>
						{SidebarData.map((item, index) => {
							return <SubMenu item={item} key={index} />;
						})}
					</SidebarWrap>
				</SidebarNav>
			</Header>
		</>
	);
};

export default Sidebar;
