import React from 'react';

import * as FaIcons from 'react-icons/fa';
import * as AiIcons from 'react-icons/ai';
import * as IoIcons from 'react-icons/io';
import * as RiIcons from 'react-icons/ri';

export const SidebarData = [
	{
		title: 'Overview',
		path: '/overview',
		icon: <AiIcons.AiFillHome />,
		iconClosed: <RiIcons.RiArrowDownSFill />,
		iconOpened: <RiIcons.RiArrowUpSFill />,
		subNav: [
			{
				title: 'Users',
				path: '/overview/users',
				icons: <IoIcons.IoIosPaper />,
			},
			{
				title: 'Revenue',
				path: '/overview/revenue',
				icons: <IoIcons.IoIosPaper />,
			},
		],
	},
	{
		title: 'Reports',
		path: '/reports',
		icon: <AiIcons.AiFillHome />,
		iconClosed: <RiIcons.RiArrowDownSFill />,
		iconOpened: <RiIcons.RiArrowUpSFill />,
		subNav: [
			{
				title: 'Reports',
				path: '/review/reports1',
				icons: <IoIcons.IoIosPaper />,
			},
			{
				title: 'Reports 2',
				path: '/review/reports2',
				icons: <IoIcons.IoIosPaper />,
			},
			{
				title: 'Reports 3',
				path: '/review/reports3',
				icons: <IoIcons.IoIosPaper />,
			},
		],
	},
	{
		title: 'Products',
		path: '/products',
		icon: <FaIcons.FaCartPlus />,
	},
	{
		title: 'Team',
		path: '/team',
		icon: <FaIcons.FaCartPlus />,
	},
	{
		title: 'Message',
		path: '/message',
		icon: <FaIcons.FaCartPlus />,
	},
];
