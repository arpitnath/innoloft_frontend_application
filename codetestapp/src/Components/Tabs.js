/* eslint-disable prettier/prettier */
import React, { useState } from 'react';
import '../App.css';
import Account from './AccountUpdate';
import Profile from './Profile';

const Tabs = () => {
	const [toggleState, setToggleState] = useState(1);

	const toggleTab = (index) => {
		setToggleState(index);
	};
	return (
		<div>
			<div className='container'>
				<div className='bloc-tabs'>
					<button
						className={toggleState === 1 ? 'tabs active-tabs' : 'tabs'}
						// eslint-disable-next-line prettier/prettier
						onClick={() => toggleTab(1)}>
						Tab 1
					</button>
					<button
						className={toggleState === 2 ? 'tabs active-tabs' : 'tabs'}
						onClick={() => toggleTab(2)}>
						Tab 2
					</button>
				</div>

				<div className='content-tabs'>
					<div
						className={
							toggleState === 1 ? 'content  active-content' : 'content'
						}>
						<h2>Account Settings</h2>
						<hr />
						<Account />
					</div>

					<div
						className={
							toggleState === 2 ? 'content  active-content' : 'content'
						}>
						<h2>User info</h2>
						<hr />
						<Profile />
					</div>
				</div>
			</div>
		</div>
	);
};

export default Tabs;
