import {
	USER_ACCOUNT_UPDATE_SUCCESS,
	USER_PROFILE_UPDATE_SUCCESS,
} from '../Constants/accountConstants';

export const updateAccount = (accountInfo) => (dispatch) => {
	fetch('https://jsonplaceholder.typicode.com/users/1', {
		method: 'PUT',
		headers: {
			'content-type': 'application/json',
		},
		body: JSON.stringify(accountInfo),
	})
		.then((res) => res.json())
		.then((accountInfo) =>
			dispatch({
				type: USER_ACCOUNT_UPDATE_SUCCESS,
				payload: accountInfo,
			})
		);
};

export const updateProfile = (profileInfo) => (dispatch) => {
	fetch('https://jsonplaceholder.typicode.com/users/1', {
		method: 'PUT',
		headers: {
			'content-type': 'application/json',
		},
		body: JSON.stringify(profileInfo),
	})
		.then((res) => res.json())
		.then((profileInfo) =>
			dispatch({
				type: USER_PROFILE_UPDATE_SUCCESS,
				payload: profileInfo,
			})
		);
};
