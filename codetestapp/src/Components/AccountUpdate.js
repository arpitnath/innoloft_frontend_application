import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { updateAccount } from '../actions/accountActions';
import {
	AcUpdateForm,
	FormGroup,
	FormLabel,
	FormInput,
	SubmitButton,
	Message,
} from './styleComponents';
import PasswordStrength from './PasswordStrength';

const AccountUpdate = () => {
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [confirmPassword, setConfirmPassword] = useState('');
	const [message, setMessage] = useState(null);

	const accountUpdate = useSelector((state) => state.accountUpdate);
	const { accountInfo } = accountUpdate;

	const dispatch = useDispatch();

	useEffect(() => {
		if (accountInfo) {
			console.log(accountInfo);
			setMessage('account updated');
		}
	}, [accountInfo]);

	const submitHandler = (e) => {
		e.preventDefault();
		if (password !== confirmPassword) {
			//   console.log('not matched');
			setMessage('Passwords do not match');
		} else {
			dispatch(updateAccount({ email, password }));
			setMessage(null);
		}
	};

	return (
		<>
			<AcUpdateForm onSubmit={submitHandler}>
				<Message>{message}</Message>
				<FormGroup>
					<FormLabel>Email</FormLabel>

					<FormInput
						type='email'
						placeholder='Enter your email'
						name='email'
						value={email}
						onChange={(e) => setEmail(e.target.value)}
					/>
				</FormGroup>

				<FormGroup>
					<FormLabel>Password</FormLabel>
					<FormInput
						type='password'
						placeholder='Enter your password'
						name='password'
						value={password}
						onChange={(e) => setPassword(e.target.value)}
					/>
					<PasswordStrength password={password} />
				</FormGroup>

				<FormGroup>
					<FormLabel>Confirm Password</FormLabel>
					<FormInput
						type='password'
						placeholder='Confirm your password'
						name='confirmPassword'
						value={confirmPassword}
						onChange={(e) => setConfirmPassword(e.target.value)}
					/>
				</FormGroup>

				<SubmitButton type='submit'>Update</SubmitButton>
			</AcUpdateForm>
		</>
	);
};

export default AccountUpdate;
