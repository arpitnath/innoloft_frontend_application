import {
	USER_ACCOUNT_UPDATE_SUCCESS,
	USER_PROFILE_UPDATE_SUCCESS,
} from '../Constants/accountConstants';

export const userAccountUpdateReducer = (state = {}, action) => {
	switch (action.type) {
		case USER_ACCOUNT_UPDATE_SUCCESS:
			return {
				...state,

				accountInfo: action.payload,
			};

		default:
			return state;
	}
};

export const userProfileUpdateReducer = (state = {}, action) => {
	switch (action.type) {
		case USER_PROFILE_UPDATE_SUCCESS:
			return {
				...state,
				profileInfo: action.payload,
			};

		default:
			return state;
	}
};
