/* eslint-disable react/prop-types */
import React from 'react';
import zxcvbn from 'zxcvbn';
import { ProgressBar, ProgressValue } from './styleComponents';

const PasswordStrength = ({ password }) => {
	const testResult = zxcvbn(password);
	const num = (testResult.score * 100) / 4;

	const createPassLabel = () => {
		switch (testResult.score) {
			case 0:
				return '';
			case 1:
				return 'Weak';
			case 2:
				return 'Medium';
			case 3:
				return 'Good';
			case 4:
				return 'Strong';
			default:
				return '';
		}
	};

	const funcProgressColor = () => {
		switch (testResult.score) {
			case 0:
				return '#828282';
			case 1:
				return '#EA1111';
			case 2:
				return '#FFAD00';
			case 3:
				return '#9bc158';
			case 4:
				return '#00b500';
			default:
				return 'none';
		}
	};

	const changePasswordColor = () => ({
		width: `${num}%`,
		background: funcProgressColor(),
		height: '7px',
	});

	return (
		<>
			<ProgressBar style={{ height: '7px' }}>
				<ProgressValue style={changePasswordColor()}></ProgressValue>
			</ProgressBar>
			<p style={{ color: funcProgressColor() }}>{createPassLabel()}</p>
		</>
	);
};

export default PasswordStrength;
